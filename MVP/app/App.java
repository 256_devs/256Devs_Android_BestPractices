package com.selecto.verdict.app;

import android.app.Application;
import android.support.annotation.StringRes;

import com.crashlytics.android.Crashlytics;
import com.selecto.verdict.di.DaggerMainComponent;
import com.selecto.verdict.di.MainComponent;
import com.selecto.verdict.di.modules.ContextModule;
import io.fabric.sdk.android.Fabric;


public class App extends Application {

    private static MainComponent mainComponent;
    private static Application application;

    public static MainComponent getMainComponent() {
        return mainComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        mainComponent = DaggerMainComponent.builder()
                .contextModule(new ContextModule(application = this))
                .build();
    }

    public static String getStringRes(@StringRes int stringId) {
        return application.getString(stringId);
    }
}
