package com.selecto.verdict.utils.exeptions;

import java.io.IOException;

public class NoInternetException extends IOException {

    @Override
    public String getMessage() {
        return "No Internet";
    }
}