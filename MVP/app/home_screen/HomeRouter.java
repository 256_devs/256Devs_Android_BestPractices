

import android.os.Bundle;
import android.support.v4.app.FragmentManager;


public class HomeRouter extends BaseRouter {

    BaseActivity activity;

    public HomeRouter(BaseActivity activity) {
        this.activity = activity;
    }



    @Override
    public void moveBackward() {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStack();
        } else {
            activity.finish();
        }
    }


    @Override
    public void moveTo(Destination dest, Bundle bundle) {
        switch (dest) {
            //case TEST:
                //avigateToFragment(activity, SomeFragment.newInstance(), true, true, false,SLIDE);
                //break;
        }
    }
}
