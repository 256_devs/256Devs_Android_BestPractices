import android.content.Context;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class GlideImage {

    @BindingAdapter({"app:image"})
    public static void setImageUrl(ImageView imageView, String url) {
        Context context = imageView.getContext();
        if(url!=null&&!url.isEmpty()) {
            Glide.with(context).load(url).into(imageView);
        }
    }
}
