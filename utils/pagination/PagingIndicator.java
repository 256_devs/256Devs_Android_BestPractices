package com.selecto.verdict.utils.pagination;

/**
 * Created by artem on 25.04.2018
 */

public class PagingIndicator {

    private Boolean isLoading = false;
    private Boolean isLastPage = false;

    public void setLoading(Boolean loading) {
        isLoading = loading;
    }

    public void setLastPage(Boolean lastPage) {
        isLastPage = lastPage;
    }

    public Boolean isLastPage() {
        return isLastPage;
    }

    public Boolean isLoading() {
        return isLoading;
    }

}
