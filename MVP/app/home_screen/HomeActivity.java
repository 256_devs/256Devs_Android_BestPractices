
import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;


import static android.view.View.VISIBLE;

public class HomeActivity extends BaseActivity<HomeContract, HomePresenter, ActivityHomeBinding> implements HomeContract, View.OnClickListener {
    private ProgressDialog dialog;

    @Override
    public ActivityHomeBinding initBinding() {
        return DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_home, null, false);
    }

    @Override
    public HomeContract getContract() {
        return this;
    }

    @Override
    public HomePresenter createPresenter() {
        return new HomePresenter();
    }

    @Override
    public BaseRouter createRouter() {
        return new HomeRouter(this);
    }

    @Override
    public BaseRouter getRouter() {
        return router;
    }

    @Override
    public void showPreloader() {
        /**
        layout vith progressBar

        if (dialog == null) {
            dialog = ProgressDialog.show(this, null, null, true);
        }
        
        dialog.setContentView(R.layout.element_progress_splash);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
        **/
    }

    @Override
    public void hidePreloader() {
        dialog.hide();
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getRouter().moveTo(FRAGMENT_FEED);
        setBottomBar();
    }

}
