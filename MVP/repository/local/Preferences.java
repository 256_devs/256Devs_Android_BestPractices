
import android.content.Context;
import android.content.SharedPreferences;



public class Preferences {

    public static final String TOKEN_KEY = "TOKEN_KEY";
    public static final String APP_PREFERENCES = "APP_PREFERENCES";

    private SharedPreferences preferences;

    public Preferences(Context context) {
        this.preferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void saveString(String key, String value) {
        preferences.edit().putString(key, value).apply();
    }

    public void saveInt(String key, int value) {
        preferences.edit().putInt(key, value).apply();
    }

    public void saveBoolean(String key, boolean value) {
        preferences.edit().putBoolean(key, value).apply();
    }

    public String getString(String key) {
        return preferences.getString(key, "");
    }

    public int getInt(String key) {
        return preferences.getInt(key, -1);
    }

    public boolean getBoolean(String key) {
        return preferences.getBoolean(key, false);
    }

    public boolean isContains(String key) {
        return preferences.contains(key);
    }

    public void clear(){preferences.edit().clear().apply();}
}
