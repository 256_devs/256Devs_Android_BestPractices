
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;


public abstract class BaseRouter {

// marker  "FRAGMENT_NAME", "ACTIVITY_NAME"
    public enum Destination {
        
    }


    public enum Animation {
        SLIDE_CARD,
        FLIP_CARD
    }

    public abstract void moveTo(Destination dest, Bundle bundle);

    public void moveTo(Destination dest) {
        moveTo(dest, null);
    }

    public void moveBackward() {

    }

//slide animation defoulte
    protected void navigateToFragment(BaseActivity activity, BaseFragment fragment, Boolean addToBackStack, Boolean isReplace, Boolean clearBackStack) {
        this.navigateToFragment(activity, fragment, addToBackStack, isReplace, clearBackStack, Animation.SLIDE_CARD);
    }

    protected void navigateToFragment(BaseActivity activity, BaseFragment fragment, Boolean addToBackStack, Boolean isReplace, Boolean clearBackStack, Animation anim) {
        FragmentManager manager = activity.getSupportFragmentManager();

        if (clearBackStack) {
            while (manager.popBackStackImmediate()) {
                Log.d("log", "popBackStack");
            }
        }

        FragmentTransaction transaction = manager.beginTransaction();
        transaction = setAnim(anim, transaction);

        if (isReplace) transaction.replace(R.id.container, fragment);
        else transaction.add(R.id.container, fragment);

        if (addToBackStack)
            transaction.addToBackStack(fragment.getTag());

        transaction.commit();

//        activity.setTitle(fragment.getTitle(activity));
    }

    private FragmentTransaction setAnim(Animation anim, FragmentTransaction transaction) {
        switch (anim) {
            case SLIDE_CARD:
                return transaction.setCustomAnimations(R.animator.slide_show_fragment_1, R.animator.slide_hide_fragment_2
                        , R.animator.slide_show_back_fragment_3, R.animator.slide_hide_back_fragment_4);
            case FLIP_CARD:
                return transaction.setCustomAnimations(R.animator.card_flip_left_in_1, R.animator.card_flip_left_out_2, R.animator.card_flip_right_in_3, R.animator.card_flip_right_out_4);
            default:
                return transaction;
        }

    }
}
