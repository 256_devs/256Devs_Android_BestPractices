

import com.selecto.verdict.app.App;
import com.selecto.verdict.repository.ApiController;
import com.selecto.verdict.repository.Preferences;
import com.selecto.verdict.repository.model.PredictionModel;

import java.lang.ref.WeakReference;

import javax.inject.Inject;


public class BasePresenter<C extends BaseContract> {
public final String TAG = this.getClass().getName();

    @Inject
    protected ApiController apiController;
    @Inject
    protected Preferences preferences;

    public BasePresenter() {
        App.getMainComponent().inject((BasePresenter<BaseContract>) this);
    }

    private WeakReference<C> contractReference = null;

    public void attachToView(C contract) {
        contractReference = new WeakReference<>(contract);
    }

    public void detachView() {
        if (contractReference != null)
            contractReference.clear();
        contractReference = null;
    }

    protected C getContract() {
        if (contractReference == null) {
            return null;
        }
        return contractReference.get();
    }
}
