package com.selecto.verdict.di;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {ApiModule.class, ContextModule.class})
public interface MainComponent {

    void inject(BasePresenter<BaseContract> cBasePresenter);


}
