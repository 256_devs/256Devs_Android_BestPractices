package com.selecto.verdict.utils.pagination;

/**
 * Created by artem on 25.04.2018
 */

public abstract class PagingCallback {
    public abstract void onDownNext();

    public void onUpNext(){}

    public void startLoading(){}

    public void stopLoading(){}
}
