#### MVP architecture template


## Please, read this [topic](https://medium.com/@cervonefrancesco/model-view-presenter-android-guidelines-94970b430ddf)
>>>
- Model: it is an interface responsible for managing data. Model’s responsibilities include using APIs, caching data, managing databases and so on. The model can also be an interface that communicates with other modules in charge of these responsibilities. For example, if you are using the Repository pattern the model could be a Repository. If you are using the Clean architecture, instead, the Model could be an Interactor.

- Presenter: the presenter is the middle-man between model and view. All your presentation logic belongs to it. The presenter is responsible for querying the model and updating the view, reacting to user interactions updating the model.

- View: it is only responsible for presenting data in a way decided by the presenter. The view can be implemented by Activities, Fragments, any Android widget or anything that can do operations like showing a ProgressBar, updating a TextView, populating a RecyclerView and so on.
>>>

## dependencies


* Follow the latest versions of libraries

```
dependencies {

//........................
//support end desigt dependencies
//........................

    //Retrofit
    implementation 'com.squareup.retrofit2:retrofit:2.3.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.3.0'
    implementation 'com.jakewharton.retrofit:retrofit2-rxjava2-adapter:1.0.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:3.9.0'

    //Dagger2
    implementation 'com.google.dagger:dagger:2.13'
    annotationProcessor 'com.google.dagger:dagger-compiler:2.13'

    //Rx2
    implementation 'io.reactivex.rxjava2:rxjava:2.1.7'
    implementation 'io.reactivex.rxjava2:rxandroid:2.0.1'
    implementation 'com.jakewharton.rxbinding2:rxbinding:2.0.0'

    //Glide
    implementation 'com.github.bumptech.glide:glide:4.6.1'
    
}
```