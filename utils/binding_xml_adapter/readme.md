#### Bindings adapter

use this adapters in xml files

## setImageUrl
```xml
                app:image="@{item.imageUrl}"
```
or
```xml
                app:image="https://www.cats.org.uk/uploads/images/featurebox_sidebar_kids/grief-and-loss.jpg"
```
or
```xml
                app:image="@string/image_url"
```
