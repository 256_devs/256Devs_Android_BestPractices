package com.selecto.verdict.utils.pagination;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.selecto.verdict.utils.Constants;

/**
 * Created by artem on 25.04.2018.
 */

public class PagingScrollListener extends RecyclerView.OnScrollListener {
    public static final int PRELOAD_ITEM_COUNT = 10;
    private PagingCallback callback;
    private PagingIndicator indicator;

    public PagingScrollListener(PagingCallback callback, PagingIndicator indicator) {
        super();
        this.callback = callback;
        this.indicator = indicator;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
        int visibleItemCount = llm.getChildCount();
        int totalItemCount = llm.getItemCount();
        int firsVisibleItemPosition = llm.findFirstVisibleItemPosition();

        if (dy > 0 && !indicator.isLoading() && !indicator.isLastPage()) {
            if ((visibleItemCount + firsVisibleItemPosition) >= totalItemCount - PRELOAD_ITEM_COUNT
                    && firsVisibleItemPosition >= 0
                    && totalItemCount >= Constants.PAGE_SIZE) {
                callback.onDownNext();
            }else if (dy< 0 &&firsVisibleItemPosition<PRELOAD_ITEM_COUNT){
                callback.onUpNext();
            }
        }
    }
}
