
import android.content.Context;


import dagger.Module;
import dagger.Provides;


@Module(includes = ContextModule.class)
public class CacheModule {

    @Provides
    public Preferences provideCache(Context context) {
        return new Preferences(context);
    }
}
