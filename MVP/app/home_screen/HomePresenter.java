

import android.util.Log;
import android.view.View;



public class HomePresenter extends BasePresenter<HomeContract> {

    public static final String TAG = HomePresenter.class.getSimpleName();

    @Override
    public void attachToView(HomeContract contract) {
        super.attachToView(contract);
    }

    @Override
    public void detachView() {
        super.detachView();
    }


    public void onNavigationItemClick(View v){
        Log.d(TAG, "onNavigationItemClick: ");
        getContract().onClick(v);
    }
}
