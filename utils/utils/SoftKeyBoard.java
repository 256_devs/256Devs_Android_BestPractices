

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by artem on 17.04.2018.
 */

public class SoftKeyBoard {

    public static void closeKeyboard(Activity activity) {
        if(activity== null) return;
        if(activity.getCurrentFocus() == null) return;

        InputMethodManager inputManager = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }
}
