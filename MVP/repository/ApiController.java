
import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;


import org.json.JSONObject;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;


public class ApiController {
    public static final String TAG = ApiController.class.getSimpleName();

    private Api Api;

    public ApiController(Api hlmApi) {
        this.Api = hlmApi;
    }

    private String getErrorMessage(ResponseBody responseBody, String field) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody.string());
            return jsonObject.getString(fils);
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public void example(Callback<ExampleResponseModel> callback) {
        verdictApi.getGeneralInterests()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        callback::onSuccess,
                        throwable -> {
                            Log.d(TAG, "getGeneralInterests: on error" + throwable.getMessage());
                            if (throwable instanceof  ConnectException) {
                                //todo
                            } else {
                                callback.onError(getErrorMessage(((HttpException) throwable).response().errorBody()), "error_message");
                            }
                        }
                );
    }
}