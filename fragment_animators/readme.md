
### xml animation for fragment transaction

File names contain numbering for the method setCustomAnimations(n1,n2,n3,n4)


### SLIDE 



### FLIP


## example

```java
activity.getSupportFragmentManager()
	.beginTransaction()
	.setCustomAnimations(R.animator.card_flip_left_in_1, R.animator.card_flip_left_out_2, R.animator.card_flip_right_in_3, R.animator.card_flip_right_out_4)
	.commit();
```