### Pagination scroll listener for RecyclerView

## PagingCallback

implement the methods that you need

* onDownNext - swipe list to forward

* onUpNext - swipe list to backward

## PagingScrollListener
if u use horisontal recycler - change all dy variables to dx in PagingScrollListener.java file
```java
if (dy > 0 
```
to
```java
if (dx > 0 
```

and
```java
if (dy< 0 
```
to
```java
if (dx< 0 
```


## PagingIndicator
This object save loading state, before fatch data u must check is loading in progress. if true just return
```java
 void getDate(int page) {
        indicator.setLoading(true);
        getContract().showPreloader();
        apiController.getDate( new Callback<Model>() {
            @Override
            public void onSuccess(Model model) {
                indicator.setLoading(false);
                if (getContract() != null) {
                    getContract().hidePreloader();
                   //do something with data
                    }
                }
            }
        }
```

## Setup

```java
binding.recycler.addOnScrollListener(new PagingScrollListener(new PagingCallback() {
            @Override
            public void onDownNext() {
                presenter.getNextData();
            }
        }, presenter.getIndicator()));
```