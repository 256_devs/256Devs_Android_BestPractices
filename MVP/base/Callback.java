

public abstract class Callback<T> {

    public abstract void onSuccess(T object);

    public void onError(String error) {
    }

    public void onStart() {}
}
