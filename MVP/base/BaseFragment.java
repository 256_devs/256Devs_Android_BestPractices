
import android.content.Context;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public abstract class BaseFragment<C extends BaseContract, P extends BasePresenter, B extends ViewDataBinding> extends Fragment implements BaseContract {


public final String TAG = this.getClass().getName();

    protected P presenter = null;
    protected B binding = null;
    protected BaseRouter router = null;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.router = createRouter();
        this.binding = initBinding(inflater);
        this.presenter = createPresenter();
        this.binding.setVariable(BR.presenter, this.presenter);
        return binding.getRoot();
    }

    public abstract B initBinding(LayoutInflater layoutInflater);

    public abstract C getContract();

    public abstract P createPresenter();

    public abstract BaseRouter createRouter();

    public abstract String getTitle(Context context);

    @Override
    public void onStart() {
        super.onStart();
        presenter.attachToView(getContract());
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.detachView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.presenter = null;
    }

    @Override
    public void showPreloader() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showPreloader();
        }
    }

    @Override
    public void hidePreloader() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hidePreloader();
        }
    }

    @Override
    public void showMessage(String message){
        SoftKeyBoard.closeKeyboard(getActivity());
        Snackbar.make(binding.getRoot(),message,Snackbar.LENGTH_LONG).show();

    }
}
