

public interface BaseContract {
    BaseRouter getRouter();

    void showPreloader();

    void hidePreloader();

    void showMessage(String message);
}
