
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


public abstract class BaseActivity<C extends BaseContract, P extends BasePresenter, B extends ViewDataBinding> extends AppCompatActivity implements BaseContract{

    public static final String TAG = BaseActivity.class.getSimpleName();


    protected Preferences preferences;

    protected P presenter = null;
    protected B binding = null;
    protected BaseRouter router = null;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.router = createRouter();
        this.binding = initBinding();
        this.presenter = createPresenter();
        this.binding.setVariable(BR.presenter, this.presenter);
        preferences = new Preferences(this);
        setContentView(binding.getRoot());
    }

    public abstract B initBinding();

    public abstract C getContract();

    public abstract P createPresenter();

    public abstract BaseRouter createRouter();

    public void setTitle(String title) {
        Log.d(TAG, "setTitle: not implemented");
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachToView(getContract());
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.detachView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.presenter = null;
    }

    @Override
    public void onBackPressed() {
        router.moveBackward();
    }

    @Override
    public void showPreloader() {
        //TODO STUB!
    }

    @Override
    public void hidePreloader() {
        //TODO STUB!
    }
}
